/*  
  FastLZ - lightning-fast lossless compression library

  Copyright (C) 2007 Ariya Hidayat (ariya@kde.org)
  Copyright (C) 2006 Ariya Hidayat (ariya@kde.org)
  Copyright (C) 2005 Ariya Hidayat (ariya@kde.org)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#ifndef FASTLZ_H
#define FASTLZ_H

#define FASTLZ_VERSION                 0x000100

#define FASTLZ_VERSION_MAJOR           0
#define FASTLZ_VERSION_MINOR           0
#define FASTLZ_VERSION_REVISION        0

#define FASTLZ_VERSION_STRING          "0.1.0"

/* The output buffer must be at least 5% larger than the input buffer and can not be smaller than 66 bytes. */
#define FASTLZ_BUFFER_PADDING(x)       (66 + (x) * 5 / 100)

#if defined (__cplusplus)
extern "C" {
#endif

#define Fastlz_O_ACCMODE   00000003          /* Mask for access mode */
#define Fastlz_O_RDONLY    00000000        /* Open for read access (only) */
#define Fastlz_O_RDOK      O_RDONLY        /* Read access is permitted (non-standard) */
#define Fastlz_O_WRONLY    00000001       /* Open for write access (only) */
#define Fastlz_O_WROK      O_WRONLY        /* Write access is permitted (non-standard) */
#define Fastlz_O_RDWR      00000002 /* Open for both read & write access */
#define Fastlz_O_CREAT     00000100        /* Create file/sem/mq object */
#define Fastlz_O_EXCL      00000200        /* Name must not exist when opened  */
#define Fastlz_O_NOCTTY    00000400               /* Required by POSIX */
#define Fastlz_O_TRUNC     00001000        /* Delete contents */
#define Fastlz_O_APPEND    00002000        /* Keep contents, append to end */

/**
  Compress a block of data in the input buffer and returns the size of 
  compressed block. The size of input buffer is specified by length. The 
  minimum input buffer size is 16.

  The output buffer must be at least 5% larger than the input buffer  
  and can not be smaller than 66 bytes.

  If the input is not compressible, the return value might be larger than
  length (input buffer size).

  The input buffer and the output buffer can not overlap.
*/

int fastlz_compress(const void* input, int length, void* output);

/**
  Decompress a block of compressed data and returns the size of the 
  decompressed block. If error occurs, e.g. the compressed data is 
  corrupted or the output buffer is not large enough, then 0 (zero) 
  will be returned instead.

  The input buffer and the output buffer can not overlap.

  Decompression is memory safe and guaranteed not to write the output buffer
  more than what is specified in maxout.
 */

int fastlz_decompress(const void* input, int length, void* output, int maxout); 

/**
  Compress a block of data in the input buffer and returns the size of 
  compressed block. The size of input buffer is specified by length. The 
  minimum input buffer size is 16.

  The output buffer must be at least 5% larger than the input buffer  
  and can not be smaller than 66 bytes.

  If the input is not compressible, the return value might be larger than
  length (input buffer size).

  The input buffer and the output buffer can not overlap.

  Compression level can be specified in parameter level. At the moment, 
  only level 1 and level 2 are supported.
  Level 1 is the fastest compression and generally useful for short data.
  Level 2 is slightly slower but it gives better compression ratio.

  Note that the compressed data, regardless of the level, can always be
  decompressed using the function fastlz_decompress above.
*/  

int fastlz_compress_level(int level, const void* input, int length, void* output);

#if defined (__cplusplus)
}
#endif

#endif /* FASTLZ_H */
